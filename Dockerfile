ARG PHP_VERSION
FROM php:$PHP_VERSION-apache

RUN export DEBIAN_FRONTEND=noninteractive \
&& apt-get update -qq \
&& apt-get install -yqq --no-install-recommends mariadb-client \
&& apt-get autoremove --purge -yqq \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/* \
&& openssl req -x509 -nodes -days 365 -newkey rsa:4096 \
-keyout /etc/ssl/private/ssl-cert-snakeoil.key \
-out /etc/ssl/certs/ssl-cert-snakeoil.pem \
-subj "/C=US/ST=Denver/L=Denver/O=Security/OU=Production/CN=localhost" \
&& docker-php-ext-install mysqli \
&& docker-php-ext-enable mysqli \
&& mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
&& a2enmod rewrite ssl \
&& a2ensite default-ssl

WORKDIR '/var/www/html'

EXPOSE 443
